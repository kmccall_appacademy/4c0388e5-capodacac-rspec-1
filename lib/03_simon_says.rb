###
#
# SIOMON SAYS
# App Academy - Prep-cohort
# Christopher Stenqvist, cc0
# 07/28/17
#
###


def echo(word)
  return word
end

def shout(sentence)
    sentence.upcase
end

def repeat(word, num=1)
  if(num == 1)
    str = word
  else
    str = ""
  end

  i=0
  while(i<num)
    str = str.to_s + " #{word}"
    i = i+1
  end
  str = str.strip

  return str
end
def start_of_word(word, num=1)
    num = num-1
    return word[0..num]
end

def first_word(sentence)
  str = sentence.split(" ")
  str[0]
end

#always uppercase first word, anything longer than 3char
def titleize(sentence)
  str = sentence.split(" ")
  str[0] = str[0].capitalize
  i=1
  while(i < str.length)
      if(str[i].length > 3 && str[i] != 'over')
          str[i] = str[i].capitalize
      end
      i = i+1
  end
  str.join(" ")
end

