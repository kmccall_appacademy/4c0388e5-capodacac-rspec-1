###
#
# CALCULATOR
# App Academy - Prep-cohort
# Christopher Stenqvist, cc0
# 07/28/17
#
###

def add(num1, num2)
  num1+num2
end

def subtract(num1,num2)
  num1-num2
end

def sum(arr)
  i=0
  sum=0
  while(i< arr.length)
    sum = sum+arr[i]
    i= i+1
  end
  return sum
end

def multiply(arr)
  i=0
  product=1
  while(i<arr.length)
    product = product*arr[i]
    i = i+1
  end
  return product
end

def power(num1, pow)
  num1**pow
end

def factorial(num)
  if(num<=0)
    return 1
  end

  i=1
  product=1
  while(i<=num)
    product = product*i 
    i = i+1
  end
  return product
end












