###
#
# PIG LATIN
# App Academy - Prep-cohort
# Christopher Stenqvist, cc0
# 07/28/17
#
###

def translate(sentence)
  words = sentence.split(" ")
  result = ""
  i=0

  while(i<words.length)
    result = result + " "  + translate_word(words[i])
    i = i + 1
  end
  result = result.strip
  result
end

def translate_word(word)
  token = word
  limit = 3
  i=0
  is_up = is_capitalized?(token)

  while(i<limit)
    if( !is_vowel(token[0]) )
      if(token[0].downcase == "q")
         token = popback(token)
         token = popback(token)
      else
        token = popback(token)
      end
    end
    i = i + 1
  end
  if(is_up)
    to_lower(token)
    token = token.capitalize
  end
  token =  add_ay(token)
  token = parse_punc(token)
end
def pop(arr)
  first = arr[0,1]
  arr[0,1] = ""
  first
end
def popback(word)
  temp = pop(word)
  word = word + temp
  word
end
def is_vowel(char)
  ['a','e','i','o','u','y'].include? char.downcase
end
def add_ay(word)
  word = word + "ay"
  word
end
def is_capitalized?(word)
  return  is_upper?(word[0])
end
def is_upper?(char)
  return char == char.upcase
end
def to_lower(word)
  word = word.downcase
end
def parse_punc(word)
  char = /[\W]/.match(word).to_s
  word = word.gsub(/[\W]/,'').to_s
  word = word.to_s + char.to_s
  word
end
