###
#
# TEMPERATURE CONVERSION
# App Academy - Prep-cohort
# Christopher Stenqvist, cc0
# 07/28/17
#
###

def ftoc(faren)
    (faren - 32.0) * (5.0/9.0)
end

def ctof(celcius)
    celcius * (1.8) + 32
end


